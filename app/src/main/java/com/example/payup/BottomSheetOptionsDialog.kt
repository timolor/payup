package com.example.payup

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomSheetOptionsDialog : BottomSheetDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        Toast.makeText(context, "In onCreateDialog", Toast.LENGTH_LONG).show()

        dialog.setOnShowListener {
            Toast.makeText(context, "dialog.setOnShowListener", Toast.LENGTH_LONG).show()
            val sheetDialog = it as BottomSheetDialog
            val bottomSheet: FrameLayout? = sheetDialog.findViewById(R.id.design_bottom_sheet)
            if (bottomSheet != null) {
                BottomSheetBehavior.from(bottomSheet).state = BottomSheetBehavior.STATE_EXPANDED
            }
        }

        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Toast.makeText(context, "onCreateView", Toast.LENGTH_LONG).show()
        return inflater.inflate(R.layout.bottom_sheet, container, false)
    }


}